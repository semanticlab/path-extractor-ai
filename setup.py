from setuptools import setup, find_packages

setup(
    name='pathExtractorAI',
    version='0.1.dev',
    author='Norman Suesstrunk',
    packages=find_packages(''),

    classifiers=[
        "Programming Language :: Python :: 3"
    ], install_requires=['keras', 'gensim', 'lxml', 'numpy', 'gensim', 'extruct', 'yaml']
)
