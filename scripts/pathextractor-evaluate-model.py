#!/usr/bin/env python3

'''
Evaluates the trained model
::author: Norman Suesstrunk
'''
import collections
import csv
import pprint

import requests
import numpy as np

from pathextractorai.ai_util import normalize_predicted_postarray
from pathextractorai.augment_features import FeatureArray, FeatureTensor
from pathextractorai.corpus_util import DEFAULT_XPATH_YAML_CONFIGS, CorpusUtil, HTML_EMBEDDING_TOOL, XPATH_CLEANUP
from pathextractorai.logging import get_logger
from pathextractorai.microdata_extractor import TEST_FORUM_DATA_MICRODATA_URL, MicroDataExtractor
from pathextractorai.pathextractor_keras_model import PathExtractorKerasModel
from pathextractorai.tree_util import get_dom_tree, get_tree_features, get_post_content, POSTS_CONTENT_DICT_KEY, \
    cleanup_trailing_redundant_tags, compare_xpath_lists, is_narrow_match, is_broader_match, compare_xpath_lists

pp = pprint.PrettyPrinter(indent=2)
logger = get_logger(__name__)

POST_PATH_CONFIGS = CorpusUtil.load_path_yaml_configs(DEFAULT_XPATH_YAML_CONFIGS)


if __name__ == '__main__':

    # load the model
    path_extractor_keras_model = PathExtractorKerasModel.load_model_file()
    logger.info("model summary: %s", path_extractor_keras_model.summary())

    # get the input shape of the model
    model_input_layer_shape = path_extractor_keras_model.get_layer(index=0).input_shape
    max_page_size = model_input_layer_shape[1]
    max_tree_len = model_input_layer_shape[2]

    # generate the feature tensors

    # counters for correctly/narrower/broader predicted paths
    correct_counter = 0;
    broader_match_counter = 0;
    narrower_match_counter = 0;

    # list to store all predicted xpaths
    predicted_xpaths = []

    # load the evaluation datas
    corpus_util = CorpusUtil()
    evaluation_data = corpus_util.read_evaluation_samples()

    # list for csv rows
    evaluation_result_rows = []

    # iterate through all evaluation_samples and predict / evaluate
    for evaluation_sample in evaluation_data:

        dom_tree = evaluation_sample['dom_tree']
        # generate the training sample
        training_sample = get_tree_features(dom_tree=dom_tree, url=evaluation_sample['url'], post_path_config=POST_PATH_CONFIGS['ppPress'])
        # generate the feature tensor
        feature_tensor = FeatureTensor.get_feature_tensor(
            training_sample,
            html_embedding_tool=HTML_EMBEDDING_TOOL,
            xpath_cleanup=XPATH_CLEANUP,
            max_page_size=max_page_size,
            max_tree_len=max_tree_len)

        feature_tensors = {'x': [], 'y': []}
        feature_tensors['x'].append(feature_tensor.tensor)
        feature_tensors['y'].append(feature_tensor.training_sample.post_array)

        # predict
        predicted_post_arrays = path_extractor_keras_model.predict(np.array(feature_tensors['x']))

        logger.info("************************")
        logger.info("Evaluation for URL=%s", evaluation_sample['url'])
        logger.info("************************")

        logger.info("gold xpath from evaluation data: %s", evaluation_sample['xpath'])

        predicted_post_array = predicted_post_arrays[0]
        logger.debug("predicted post array: %s", predicted_post_array)
        normalized_predicted_post_array = normalize_predicted_postarray(predicted_post_array)
        logger.debug("normalized predicted post array: %s", normalized_predicted_post_array)

        # predict the unique xpath from the predicted xpaths (heuristic)
        logger.info("---------")
        most_common_xpath = PathExtractorKerasModel.predict_unique_xpath(training_sample.xpath_array, normalized_predicted_post_array)
        logger.info("most common xpath (predicted): %s", most_common_xpath)
        predicted_xpaths.append(most_common_xpath)

        # split and clean based on trailing tags in blacklist
        most_common_xpath = "/".join(cleanup_trailing_redundant_tags(most_common_xpath.split("/")))
        logger.info("most common xpath (cleaned): %s", most_common_xpath)
        logger.info("---------")

        # post content from gold xpath
        gold_post_contents = get_post_content(dom_tree, evaluation_sample['xpath'])
        logger.debug("gold posts contents: %s", gold_post_contents)
        if gold_post_contents:
            for gold_post_content in gold_post_contents[POSTS_CONTENT_DICT_KEY]:
                gold_post_content = " ".join(gold_post_content.strip().split())
                evaluation_result_row = [evaluation_sample['url'], gold_post_content, ' ', evaluation_sample['xpath'], most_common_xpath]
                evaluation_result_rows.append(evaluation_result_row)

        # posts contents from predicted xpaths
        predicted_post_contents = get_post_content(dom_tree, most_common_xpath)
        logger.debug("predicted posts contents: %s", predicted_post_contents)
        if predicted_post_contents:
            for predicted_post_content in predicted_post_contents[POSTS_CONTENT_DICT_KEY]:
                predicted_post_content = " ".join(predicted_post_content.strip().split())
                evaluation_result_row = [evaluation_sample['url'], ' ', predicted_post_content, evaluation_sample['xpath'], most_common_xpath]
                evaluation_result_rows.append(evaluation_result_row)

        # check if the predicted xpath is contained in the gold xpaths derived from the microdata
        if most_common_xpath == evaluation_sample['xpath']:
            logger.info("correctly predicted xpath: %s", most_common_xpath)
            correct_counter = correct_counter + 1


        # check narrow_match / broader_match, is_mismatch

        xpath_splitted_predicted = most_common_xpath.split("/");
        xpath_evaluation_gold = evaluation_sample['xpath'].split("/")

        mismatch, missing, added = compare_xpath_lists(xpath_evaluation_gold, xpath_splitted_predicted)
        narrow_match, diff = is_narrow_match(missing, added)

        if narrow_match:
            logger.info("narrow match: predicted: \n%s\ngold: \n%s\ndiff %s", xpath_splitted_predicted, xpath_evaluation_gold, diff)
            narrower_match_counter = narrower_match_counter + 1

        broader_match, diff = is_broader_match(missing, added)
        if broader_match:
            logger.info("broader match: predicted: \n%s\ngold: \n%s\ndiff %s", xpath_splitted_predicted, xpath_evaluation_gold, diff)
            broader_match_counter = broader_match_counter + 1

    # write the results to a csv
    with open('pathextractor-predictionresults.csv', mode='w') as prediction_csv_file:
        prediction_writer = csv.writer(prediction_csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        prediction_writer.writerow(['url', 'post_text_gold', 'post_text_predicted', 'xpath_gold', 'xpath_predicted'])
        for evaluation_row in evaluation_result_rows:
            prediction_writer.writerow(evaluation_row)

    logger.info("csv written to %s", prediction_csv_file)


    xpath_counter = collections.Counter(predicted_xpaths)

    logger.info("correct predicted: %s, total urls predicted: %s", correct_counter, len(evaluation_data))
    logger.info("narrow matches: %s, broader matches: %s", narrower_match_counter, broader_match_counter)
    logger.info("predicted xpath statistics: %s", xpath_counter)
    exit(0)
