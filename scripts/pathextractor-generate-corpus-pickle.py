#!/usr/bin/env python3

'''
Reads the complete forum and generates the pickle file

::author: Norman Suesstrunk
'''
import os
from pathextractorai.corpus_util import CorpusUtil

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
TEST_FORUM_DATA_DIR = SCRIPT_DIR + "/../tests/pathextractor_ai_tests/test_forum_data"

from pathextractorai.logging import get_logger
logger = get_logger(__name__)

if __name__ == '__main__':

    max_page_size = 700
    max_tree_len = 50

    logger.info("starting generating pickle corpus with max_page_size=%s, max_tree_length=%s", max_page_size, max_tree_len)

    # use the small forum test data
    # corpus_util = CorpusUtil(TEST_FORUM_DATA_DIR)

    # use the full corpus
    corpus_util = CorpusUtil(augmentation_count=200)

    # read all the html files and generate feature arrays
    augmented_training_samples = corpus_util.read_forum_data_to_augmented_training_samples()
    logger.info("number of generated training samples: %s", len(augmented_training_samples))

    # generate the training tensors
    feature_tensors = CorpusUtil.generate_feature_tensors(
        training_samples=augmented_training_samples,
        max_page_size=max_page_size,
        max_tree_len=max_tree_len)

    # dump the training tensors
    CorpusUtil.pickle_dump_feature_tensors(feature_tensors=feature_tensors)