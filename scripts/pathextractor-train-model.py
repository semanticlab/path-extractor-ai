#!/usr/bin/env python3

'''
Trains the forum extraction component

::author: Norman Suesstrunk
'''

import pprint
import os
import numpy as np
from pathextractorai.corpus_util import CorpusUtil
from pathextractorai.logging import get_logger
from pathextractorai.pathextractor_keras_model import trainAndEvaluateModel

pp = pprint.PrettyPrinter(indent=2, width=800)
logger = get_logger(__name__)

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
TEST_FORUM_DATA_DIR = SCRIPT_DIR + "/../tests/pathextractor_ai_tests/test_forum_data"

if __name__ == '__main__':

    max_page_size = 700
    max_tree_length = 30

    # use the full corpus
    # corpus_util = CorpusUtil(augmentation_count=50)

    # use the forum test data
    corpus_util = CorpusUtil(forum_data_dir=TEST_FORUM_DATA_DIR, augmentation_count=50)

    # read all the html files and generate feature arrays
    augmented_training_samples = corpus_util.read_forum_data_to_augmented_training_samples()
    logger.info("number of generated training samples: %s", len(augmented_training_samples))

    # generate the training tensors
    feature_tensors = CorpusUtil.generate_feature_tensors(training_samples=augmented_training_samples,
                                                          max_page_size=max_page_size,
                                                          max_tree_len=max_tree_length)

    # assert dimensions for the x-samples
    result_shape_x = (len(augmented_training_samples), max_page_size, max_tree_length, 20)
    # logger.info("tensors x shape %s", feature_tensors['x'].shape)
    assert feature_tensors['x'].shape == result_shape_x

    # assert the dimensions for the y-samples
    result_shape_y = (len(augmented_training_samples), max_page_size)
    # logger.info("tensors y shape %s", feature_tensors['y'].shape)
    assert feature_tensors['y'].shape == result_shape_y
    for feature_tensor_y in feature_tensors['y']:
        # logger.info(np.array(feature_tensor_y).shape)
        assert np.array(feature_tensor_y).shape == (max_page_size, )

    # train & evaluate
    trainAndEvaluateModel(
        feature_tensors=feature_tensors, epochs=3, max_tree_length=max_tree_length, max_page_size=max_page_size)