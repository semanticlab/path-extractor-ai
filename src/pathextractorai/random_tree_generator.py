#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on Jun 22, 2019

codeauthor: Norman Suesstrunk <n.suesstrunk@gmail.com>
'''

import pprint
import random

from lxml import etree

from pathextractorai.logging import logger

pp = pprint.PrettyPrinter(indent=2)


class RandomTreeGenerator():
    VALID_PARENT_ELEMENTS = ['div', 'p']

    HTML_ELEMENTS = ['div', 'p', 'span', 'b', 'a', 'link', 'pre', 'strong',
                     'title']

    MAX_TREE_DEPTH = 8

    MAX_CHILDS = 6

    MAX_LIST_ELEMENTS = 8

    MAX_DIV_ELEMENTS = 4
    MAX_DIV_DEPTHS = 3

    def __init__(self, etree_root):
        self.dom_generating_functions = {}
        self.init_dom_generation_lists()
        self.etree_root = etree_root

    def init_dom_generation_lists(self):
        self.dom_generating_functions[
            'generate_html_list'] = self.generate_html_list
        self.dom_generating_functions[
            'generate_embedded_divs'] = self.generate_embedded_divs

    def generate_random_tree(self):
        logger.debug('generating embedded divs')

        # select random tree depth
        max_tree_depth = random.randrange(1, self.MAX_TREE_DEPTH)
        logger.debug("random tree depth: %s", max_tree_depth)

        for function_key in self.dom_generating_functions:
            part_tree = self.dom_generating_functions[function_key]()

        # select random parent element

    def generate_html_list(self):
        ul_element = self.etree_root.makeelement(_tag="ul")
        number_of_list_elements = random.randrange(1, self.MAX_LIST_ELEMENTS)
        for i in range(number_of_list_elements):
            logger.debug('i %s', i)
            li_element = self.etree_root.makeelement(_tag="li")
            ul_element.append(li_element)
        logger.debug("generated html list: %s",
                     etree.tostring(ul_element, pretty_print=True))
        return ul_element

    def generate_embedded_divs(self):
        """generates embedded divs

            Returns:
            embedded divs as html_element object

        """
        # make first parent div:
        root_div_element = self.etree_root.makeelement(_tag='div')

        div_leaves = []
        div_leaves.append(root_div_element)
        depth = 0;

        while depth < self.MAX_DIV_DEPTHS:
            # generate child divs

            new_child_divs = []

            # add the new divs to the child leaves
            for div_element in div_leaves:
                number_of_child_divs = random.randrange(1, self.MAX_DIV_ELEMENTS)
                for i in range(number_of_child_divs):
                    div_child_element = self.etree_root.makeelement(_tag="div")
                    div_element.append(div_child_element)
                    new_child_divs.append(div_child_element)
            # increase depth
            div_leaves = new_child_divs
            depth = depth + 1

        return root_div_element
