#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Jun 7, 2019

.. codeauthor: Norman Süsstrunk <norman.suesstrunk@htwchur.ch>
'''
import pprint
import extruct
import requests

from pathextractorai.logging import get_logger

pp = pprint.PrettyPrinter(indent=2)
from w3lib.html import get_base_url
from pathextractorai.tree_util import get_dom_tree, get_tree_features, get_post_content, element_to_tag_list, \
    dom_tree_to_xpaths, clean_xpath

TEST_FORUM_DATA_MICRODATA_URL = [
    'https://forum.parkinsons.org.uk/t/i-think-i-have-parkinsons-your-best-advice-please/20894/7',
    'https://forum.parkinsons.org.uk/t/stopping-pd-progression/19184',
    'https://forum.parkinsons.org.uk/t/dopamine-agonists-and-catastrophic-obsessive-compulsive-disorders/5335',
    'https://forum.parkinsons.org.uk/t/where-we-are-at-and-where-we-are-going-at-parkinsons-uk/3760'
]

logger = get_logger(__name__)


class MicroDataExtractor(object):

    def __init__(self, url):
        self.url = url
        request = requests.get(self.url)
        base_url = get_base_url(request.text, request.url)
        self.dom_tree = get_dom_tree(request.text);
        self.data = extruct.extract(request.text, base_url=base_url)

    def extract_posts(self):
        posts = []
        for post in self.data['microdata']:
            extracted_post = self.get_post(post)
            if extracted_post:
                posts.append(self.get_post(post))
        return posts

    def get_post(self, post):
        if 'type' in post:

            if post['type'] == 'http://schema.org/DiscussionForumPosting':

                if ('properties' in post) & ('articleBody' in post['properties']):

                    post_content = None
                    if 'articleBody' in post['properties']:
                        post_content = post['properties']['articleBody']

                    post_url = None
                    if 'position' in post['properties']:
                        post_url = post['properties']['position']

                    post_author = None
                    if 'author' in post['properties']:
                        post_author = post['properties']['author'].get(
                            'properties', {}).get('name', None)
                    metadata = {'user_name': post_author}

                    post_date = None
                    if 'datePublished' in post['properties']:
                        post_date = post['properties']['datePublished']

                    return {
                        'post': post_content,
                        'date': post_date,
                        'url': post_url,
                        'metadata': metadata,
                        'url': self.url}

    def getXpathsForMicrodataPosts(self):
        """
        iterates through every post extracted from the microdata and tries to map it to the corresponding xpath
        :return:
        a list of xpaths that describe a post
        """

        post_xpaths = []

        # extract all the contents for the xpaths
        self.extractXpathContents()

        posts = self.extract_posts()
        for post in posts:
            cleaned_post_content = " ".join(post['post'].split())
            logger.debug("microdata post: %s", cleaned_post_content)
            for xpath, xpath_contents in self.element_content_dict.items():
                for xpath_content in xpath_contents:
                    # compare content under
                    if cleaned_post_content == xpath_content:
                        post_xpaths.append(xpath)
                        logger.debug("found xpath for post content: %s", xpath)

            logger.debug("-----------------------")
        return set(post_xpaths)


    def extractXpathContents(self):
        """
        calculates the text content for every xpath
        and stores it with the corresponding xpath as dict-key

        :return:

        Example:

        {
            ...
            '/html/body/div': ['lorem ipsum', 'lorem ipsum']
            '/html/body/div/div': ['lorem ipsum', 'lorem ipsum...']
            ...
        }

        """
        self.element_content_dict = {}

        xpath_arrays = dom_tree_to_xpaths(self.dom_tree)
        for xpath in xpath_arrays:

            # construct the xpath as string representation
            xpath_serialized = "/" + "/".join(xpath).replace("*", "")

            # remove the trailing slash "/"
            last_char = xpath_serialized[-1:]
            if last_char == "/":
                xpath_serialized = xpath_serialized[0:len(xpath_serialized)-1]

            # add it to the result dict
            if xpath_serialized not in self.element_content_dict:
                self.element_content_dict[xpath_serialized] = []

            # get the content for the xpath and store it in the corresponding xpath dict key
            elements_content = get_post_content(self.dom_tree, xpath_serialized)
            if elements_content:
                for element_content in elements_content['posts_content']:
                    cleaned_content = " ".join(element_content.split())
                    if cleaned_content != '':
                        self.element_content_dict[xpath_serialized].append(cleaned_content)

        return self.element_content_dict;


