#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
codeauthor: Norman Suesstrunk <norman.suesstrunk@htwchur.ch>
'''

from keras.engine.saving import load_model
from keras.layers import Dense, Conv2D, Flatten
from keras.models import Sequential
import numpy as np
import pprint

from pathextractorai.logging import get_logger

pp = pprint.PrettyPrinter(indent=2, width=800)


from pathextractorai.ai_util import normalize_predicted_postarray, compare_post_arrays, EvaluationItem
from pathextractorai.augment_features import DEFAULT_MAX_PAGE_SIZE, DEFAULT_MAX_TREE_LEN, FeatureArray
logger = get_logger(__name__)

MODEL_FILE_NAME = 'pathextractor_keras_model'
DEFAULT_EPOCHS = 3

class PathExtractorKerasModel(object):

    def __init__(self, model=None, page_size = DEFAULT_MAX_PAGE_SIZE, tree_len=DEFAULT_MAX_TREE_LEN):
        logger.info('init')
        if not model:
            self.model = PathExtractorKerasModel.init_model(page_size= page_size, tree_len = tree_len)
        else:
            self.model = model

    def train_model(self, numpy_x, numpy_y, epochs=DEFAULT_EPOCHS):
        self.model.fit(numpy_x, numpy_y, epochs=epochs)

    def predict(self, numpy_x):
        return self.model.predict(numpy_x);

    def save_model(self, file_name=MODEL_FILE_NAME):
        # save the model
        self.model.save(file_name)
        logger.info('keras model saved to %s', file_name)

    @staticmethod
    def init_model(page_size, tree_len):
        model = Sequential()
        # add model layers
        model.add(
            Conv2D(8, kernel_size=3, activation='relu',
                   input_shape=(page_size, tree_len, 20)))
        model.add(Conv2D(4, kernel_size=3, activation='relu'))
        model.add(Flatten())
        model.add(Dense(page_size, activation='softmax'))
        # compile the model
        model.compile(optimizer='adam', loss='categorical_crossentropy',
                      metrics=['accuracy'])
        logger.info("model summary: %s", model.summary())
        return model

    @staticmethod
    def load_model_file(file_name=MODEL_FILE_NAME):
        # load the model
        return load_model(file_name)


    @staticmethod
    def predict_unique_xpath(xpath_array, predicted_post_array):
        post_xpath_counter = FeatureArray.counted_post_xpaths(xpath_array, predicted_post_array)
        logger.info("post_xpath_counter=%s", post_xpath_counter)
        # get the most common xpath (xpath with the highest count)
        xpath_most_common = post_xpath_counter.most_common(1)
        if xpath_most_common:
            xpath_most_common = xpath_most_common[0][0]
            # clean up the xpath from "***"
            xpath_most_common = xpath_most_common.replace("*", "")
            # remove the last "/" if any
            last_string_char = xpath_most_common[-1:]
            if last_string_char == "/":
                xpath_most_common = xpath_most_common[:len(xpath_most_common) - 1]
            return xpath_most_common
        return None

def trainAndEvaluateModel(feature_tensors, epochs=5, max_page_size=DEFAULT_MAX_PAGE_SIZE, max_tree_length=DEFAULT_MAX_TREE_LEN):
    # train the model

    pathextractor_keras_model = PathExtractorKerasModel(
        page_size=max_page_size,
        tree_len=max_tree_length)
    logger.info("model summary: %s", pathextractor_keras_model.model.summary())
    pathextractor_keras_model.train_model(numpy_x=feature_tensors['x'],
                                          numpy_y=feature_tensors['y'],
                                          epochs=epochs)
    # save the model
    pathextractor_keras_model.save_model()

    # predict
    post_array_predictions = pathextractor_keras_model.predict(feature_tensors['x'])


    eval_items = []

    # iterate through all y-tensors
    for index, feature_tensor in enumerate(feature_tensors['x']):

        original_training_sample = feature_tensors['training_samples'][index]

        # the corresponding predicted post array
        predicted_post_array = post_array_predictions[index]

        # the corresponding post array from the training tensor
        original_post_array = feature_tensors['y'][index]

        logger.debug("predicted post array: ", predicted_post_array)
        # normalize the predicted post_array
        normalized_predicted_post_array = normalize_predicted_postarray(predicted_post_array)
        # logger.debug("normalized predicted post array: %s", normalized_predicted_post_array)
        # logger.debug("original post array: %s", original_post_array)

        # calculate the similarity between predicted post_array and the gold_standard_post_array
        post_array_similarity = compare_post_arrays(normalized_predicted_post_array, original_post_array)
        logger.info("similarity of predicted post array to original post array: %s", post_array_similarity)
        eval_item = EvaluationItem.get_evaluation_item_for_post_arrays(np.array(original_post_array),
                                                                    np.array(normalized_predicted_post_array))
        eval_items.append(eval_item)

        logger.info("tp=%s, fp=%s, tn=%s, fn=%s, precision=%s, recall=%s, f1=%s",
                    eval_item.tp,
                    eval_item.fp,
                    eval_item.tn,
                    eval_item.fn,
                    eval_item.precision,
                    eval_item.recall,
                    eval_item.f1)

        # print out counted post xpaths with predicted post array
        post_xpath_counter = FeatureArray.counted_post_xpaths(original_training_sample.xpath_array,
                                                              predicted_post_array)
        logger.info("predicted xpaths: %s", pp.pformat(post_xpath_counter))
        logger.info("---------------------------------")

    # calculate the mean / average eval values over all eval_items
    mean_eval = EvaluationItem.mean_eval_from_eval_items(eval_items)
    logger.info("---------------------------------------------------------------------------------------")
    logger.info("mean evaluation: precision=%s, recall=%s, f1=%s, tp=%s, fp=%s, tn=%s, fn=%s, ",
                    mean_eval.precision,
                    mean_eval.recall,
                    mean_eval.f1,
                    mean_eval.tp,
                    mean_eval.fp,
                    mean_eval.tn,
                    mean_eval.fn
                    )