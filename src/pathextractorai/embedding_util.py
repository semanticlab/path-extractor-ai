'''
Common operations on html embeddings

::author: Albert Weichselbraun
'''
import csv
import gzip
from random import uniform
from gensim.models import Word2Vec
import os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
VALID_HTML_VOC_FILE = SCRIPT_DIR + '/html_vocabulary.cvs.gz'
HTML_EMBEDDING = Word2Vec.load(SCRIPT_DIR + '/../../html.model.gz')

class HtmlEmbeddingTool(object):

    def __init__(self):
        self.embedding = HTML_EMBEDDING

    def get_replacement(self, word):
        # assemble all choices and their corresponding probabilities
        total_p = sum([choice[1] for choice in self.embedding.most_similar(word)])
        chosen_p = uniform(0, total_p)

        sum_p = 0
        for term, p in self.embedding.most_similar(word):
            sum_p += p
            if sum_p >= chosen_p:
                return term

        raise ValueError("Get replacement sampling does not work.")

    def __getitem__(self, k):
        return self.embedding[k]

    def __len__(self):
        '''
        Returns
        -------
        The embedding size
        '''
        return self.embedding.vector_size


def load_valid_html_tags():
    valid_html_tags = []
    with gzip.open(VALID_HTML_VOC_FILE, mode='rt') as valid_html_tag_file:
        csv_reader = csv.reader(valid_html_tag_file)
        for valid_html_csv_row in csv_reader:
            valid_html_tags.append(valid_html_csv_row[0])
    return valid_html_tags
