#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
.. codeauthor: Norman Suesstrunk
'''

# import logging & create logger handlers
import logging
import os
from datetime import datetime
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))

console_handler = logging.StreamHandler()
log_file = SCRIPT_DIR + '/../../logs/pathextractor_ai_' + datetime.now().strftime("%Y_%m_%d-%H:%M:%S") + '.log'
file_handler = logging.FileHandler(log_file, mode='w')

# clean logging handlers because tensorflow messes with the logging (https://stackoverflow.com/questions/56984333/tensorflow-suppresses-logging-messages-bug)
logging.getLogger().handlers = []
logging.basicConfig(format='%(name)s - %(levelname)s - %(message)s', level=logging.INFO)

# create a custom logger and add the handlers
logger = logging.getLogger(__name__)
logger.addHandler(console_handler)
logger.addHandler(file_handler)

def get_logger(logger_name):
    return logger






