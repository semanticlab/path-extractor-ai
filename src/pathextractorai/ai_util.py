#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
.. codeauthor: Norman Süsstrunk <norman.suesstrunk@htwchur.ch>
'''
import collections

import numpy as np

def normalize_predicted_postarray(numpy_post_array):
    '''
    numpy_post_array: 1-dimensional numpy array

    Returns
    -------
    normalized array as following:
        element = 0 -> normalized to -1
        element > 0 -> normalized to 1
    ...
    '''
    with np.nditer(numpy_post_array, op_flags=['readwrite']) as it:
        for x in it:
            if x == 0:
                x[...] = -1
            else:
                x[...] = 1

    return numpy_post_array


def compare_post_arrays(numpy_post_array_l, numpy_post_array_r):
    close_array = np.isclose(numpy_post_array_l, numpy_post_array_r)
    counted = dict(collections.Counter(close_array))
    # print(numpy_post_array_l.size)
    # print(type(counted))
    # print(counted[True])

    if True not in counted:
        return 0

    return counted[True] / numpy_post_array_l.size


class EvaluationItem(object):

    def __init__(self, tp, fp, tn, fn, precision, recall, f1):
        self.tp = tp
        self.fp = fp
        self.tn = tn
        self.fn = fn
        self.precision = precision
        self.recall = recall
        self.f1 = f1

    @staticmethod
    def get_evaluation_item_for_post_arrays(numpy_post_array_gold, numpy_post_array_compare):

        tp = 0;
        fp = 0;
        tn = 0;
        fn = 0;
        precision = 0
        recall = 0
        f1 = 0

        for post_val_gold, post_val_compare in np.nditer([numpy_post_array_gold, numpy_post_array_compare]):

            if post_val_gold == 1 and post_val_compare == 1:
                tp += 1
            elif post_val_gold == -1 and post_val_compare == -1:
                tn += 1
            elif post_val_gold == -1 and post_val_compare == 1:
                fp += 1
            elif post_val_gold == 1 and post_val_compare == -1:
                fn += 1
        if tp + fp != 0:
            precision = tp / (tp + fp)

        if tp + fn != 0:
            recall = tp / (tp + fn)
        if precision + recall != 0:
            f1 = 2 * ((precision * recall) / (precision + recall))

        return EvaluationItem(tp=tp, fp=fp, tn=tn, fn=fn, precision=precision, recall=recall, f1=f1)

    @staticmethod
    def mean_eval_from_eval_items( eval_items):

        tp_sum = 0;
        fp_sum = 0;
        tn_sum = 0;
        fn_sum = 0;
        precision_sum = 0
        recall_sum = 0
        f1_sum = 0

        for eval_item in eval_items:
            tp_sum += eval_item.tp
            fp_sum += eval_item.fp
            tn_sum += eval_item.tn
            fn_sum += eval_item.fn
            precision_sum += eval_item.precision
            recall_sum += eval_item.recall
            f1_sum += eval_item.f1

        len_eval_items = len(eval_items)

        return    EvaluationItem(
            tp = tp_sum / len_eval_items,
            fp = fp_sum / len_eval_items,
            tn = tn_sum / len_eval_items,
            fn = fn_sum / len_eval_items,
            precision = precision_sum / len_eval_items,
            recall = recall_sum / len_eval_items,
            f1 = f1_sum / len_eval_items
        )