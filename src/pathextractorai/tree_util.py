'''
creates trees from html pages
'''
import pprint
import re
from html import unescape
import lxml
from lxml import etree
from pathextractorai.augment_features import TrainingSample
from pathextractorai.logging import get_logger

pp = pprint.PrettyPrinter(indent=2)
logger = get_logger(__name__)

RE_FILTER_XML_HEADER = re.compile("<\?xml version=\".*? encoding=.*?\?>")
# HTML elements to ignore for text computations
TEXT_BLACKLIST = ('script', 'style')

POSTS_CONTENT_DICT_KEY = 'posts_content'
XPATH_DICT_KEY = 'xpath'


def get_dom_tree(html):
    '''
    Returns the dom tree for the given HTML file.
    '''
    html = RE_FILTER_XML_HEADER.sub("", html)
    return etree.HTML(html)


def get_tree_features(dom_tree, url, post_path_config):
    '''
    Returns the TrainingSample for the dom_tree

    example of a returned training sample:
         (
             xpath_array = [[html], ['html', 'head', 'title', '*****']['html','body','ul','li[post-content]']],
             post_array  = [0, 0, 1]
         )
    '''

    # get all post_content elements with known xpath
    post_elements = dom_tree.xpath(post_path_config.post_content)
    post_xpath_with_childs_list = []
    for post_element in post_elements:
        post_xpath_with_childs_list.append(post_element)

    xpath_array = []
    post_array = []
    for dom_element in dom_tree.iter():
        if (isinstance(dom_element.tag, str)):
            element_list = element_to_tag_list(dom_element)
            xpath_array.append(element_list)

            # check if the current dom_element is a post_element or a child
            if dom_element in post_xpath_with_childs_list:
                post_array.append(1)
            else:
                post_array.append(-1)

    return TrainingSample(xpath_array=xpath_array, post_array=post_array, url=url)


def dom_tree_to_xpaths(dom_tree):
    xpath_array = []
    for dom_element in dom_tree.iter():
        if (isinstance(dom_element.tag, str)):
            element_list = element_to_tag_list(dom_element)
            xpath_array.append(element_list)
    return xpath_array


def element_to_tag_list(dom_element):
    '''
    Returns the element list represenation for a etree dom element
    '''
    xpath = dom_element.getroottree().getpath(dom_element)
    if not isinstance(dom_element.tag, str):
        raise ValueError("Cannot process HTML comments.")

    element_list = [element.split("[")[0] for element in xpath.split("/")][1:]
    element_text = dom_element.text.strip() if dom_element.text else ""
    if not dom_element.tag in TEXT_BLACKLIST and element_text:
        element_list.append(normalize_dom_text(element_text))
    return element_list


def get_post_content(dom_tree, xpath):
    '''
        extract all text from the elements defined by the xpath

        returns a dict with
             {
            POSTS_CONTENT_DICT_KEY: [list_of_text_content],
            XPATH_DICT_KEY: [the xpath the elements where found for]
            }
    '''
    posts_content = []
    try:
        post_elements = dom_tree.xpath(xpath)
        # logger.debug('using xpath to extract post content %s', xpath)
        if post_elements:
            for post_element in post_elements:
                posts_content.append(''.join(post_element.itertext()).strip())
            return {
                POSTS_CONTENT_DICT_KEY: posts_content,
                XPATH_DICT_KEY: xpath
            }
        return None
    except lxml.etree.XPathEvalError:
        logger.warn("the xpath %s is not valid", xpath)
    except TypeError as type_error:
        logger.warn(type_error)


def clean_xpath(xpath):
    '''
    cleans the given xpath:
    - removes '*'
    - removes trailing '/' if any
    '''
    xpath = xpath.replace("*", "")
    if xpath[-1] == "/":
        xpath = xpath[:-1]
    return xpath


TRAILING_BLACKLIST = ['p', 'br', 'a', 'b', 'u', 'i','strong','emph','img']
def cleanup_trailing_redundant_tags(xpath):
    # iterate list backwards
    for tag in reversed(xpath):
        if tag in TRAILING_BLACKLIST:
            xpath.pop()
        else:
            break
    return xpath

def is_narrow_match(missing, added):
    if len(missing) > 0 and len(added) ==0:
        return True, missing
    else:
        return False, []

def is_mismatch(missing, added):
    if len(missing)>0 and len(added) > 0:
        return True
    return False

def is_broader_match(missing, added):
    if len(missing) == 0 and len(added) > 0:
        return True, added
    else:
        return False, []

def compare_xpath_lists(ref_xpath_lst, compare_xpath_lst):
    ref_xpath_lst_len = len(ref_xpath_lst)
    compare_xpath_lst_len = len(compare_xpath_lst)

    missing = []
    added = []
    mismatch = False

    if ref_xpath_lst_len > compare_xpath_lst_len:
        for i in range(compare_xpath_lst_len):
            if ref_xpath_lst[i] != compare_xpath_lst[i]:
                mismatch = True
        missing = ref_xpath_lst[i+1:]

    if ref_xpath_lst_len < compare_xpath_lst_len:
        for i in range(ref_xpath_lst_len):
            if ref_xpath_lst[i] != compare_xpath_lst[i]:
                mismatch = True
        added = compare_xpath_lst[i+1:]

    if ref_xpath_lst_len == compare_xpath_lst_len:
        for i in range(ref_xpath_lst_len):
            if ref_xpath_lst[i] != compare_xpath_lst[i]:
                mismatch = True

    return mismatch, missing, added


def normalize_dom_text(txt):
    '''
    Normalizes text based on its length
    '''
    txt = unescape(txt).strip()
    if not txt:
        return ""
    elif len(txt) <= 5:
        return "*"
    elif len(txt) <= 10:
        return "**"
    elif len(txt) <= 20:
        return "***"
    elif len(txt) <= 40:
        return "****"
    return "*****"
