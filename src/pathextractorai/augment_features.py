"""
Created on Jun 7, 2019

.. codeauthor: Norman Suesstrunk <norman.suesstrunk@htwchur.ch>

Augments tree features by

 1. inserting subtrees
 2. increasing the number of repeated elements (i.e. the number of posts)
 3. swapping html tags (e.g. ul > ol, ul > div, ...)

"""

from collections import namedtuple, Counter
from enum import Enum
from itertools import chain, repeat, islice
from random import randint, choice
import pprint
pp = pprint.PrettyPrinter(indent=4,width=200)

from pathextractorai.logging import get_logger
logger = get_logger(__name__)

SEPARATOR_ELEMENT_NAME = '[SEP]'

TrainingSample = namedtuple('TrainingSample',
                            ['xpath_array', 'post_array', 'url'])

class FeatureArray(object):
    '''
    Handles and augments TrainingSamples.
    '''

    @staticmethod
    def replace_tag(training_sample, language_model):
        replace_xpath, prefix, suffix = choose_path(training_sample.xpath_array, prefix_suffix_len=5)
        if replace_xpath:
            # TODO: hack - we chose tag_size=2 because the estimate always includes the [SEP]-tag at the end
            tags_estimate = language_model.estimate(prefix, suffix, gap_size=2)
            replacement_tag = tags_estimate[0]
            logger.debug("replace tag: replace_xpath=%s, prefix=%s, suffix=%s tags_estimate=%s, replacement_tag=%s",replace_xpath, prefix, suffix, tags_estimate, replacement_tag)
            return FeatureArray.replace_html_tag(training_sample, replace_xpath, replacement_tag)
        return training_sample

    @staticmethod
    def insert_sequence(training_sample, language_model):
        gap_size = choice(language_model.gap_sizes)
        insert_xpath, prefix, suffix = choose_path(training_sample.xpath_array, prefix_suffix_len=5)
        insert_sequence = language_model.estimate(prefix, suffix, gap_size=gap_size)
        # TODO: dirty hack - the generated insert sequence always contains the [SEP]-tag at the end -> we remove it here
        adjusted_insert_sequence = insert_sequence[0:len(insert_sequence)-1]
        logger.debug("insert_sequence: %s, prefix: %s, suffix %s, insert sequence: %s, adjusted insert sequence: %s", insert_xpath, prefix, suffix, insert_sequence, adjusted_insert_sequence)
        return FeatureArray.insert_tag_sequence(training_sample, insert_xpath, adjusted_insert_sequence)

    @staticmethod
    def counted_post_xpaths(xpath_array, post_array):
        separator = '/'
        posts_arrays = []
        for index, item in enumerate(post_array):
            if item == 1:
                try:
                    posts_arrays.append(
                        separator + separator.join(
                            xpath_array[index]))

                # here, the xpath array is not "stretched" to the max_tree_length
                except IndexError:
                    # print("index "+ str(index)+" out of range in xpath_array with length " +str(len(self.training_sample.xpath_array)))
                    logger.debug('index %s out of range in xpath_array with length %s',
                                 index,
                                 len(xpath_array))
        return Counter(posts_arrays)

    @staticmethod
    def get_first_post_xpath(training_sample):
        for xpath, post in zip(training_sample.xpath_array, training_sample.post_array):
            if post:
                return xpath

        logger.warning('Cannot find a post in training sample with URL: %s', training_sample.url)
        return None

    @staticmethod
    def insert_tag_sequence(training_sample, insert_xpath, insert_sequence):
        '''
        Returns
        -------
        A copy of training_sample with insert_sequence inserted into
        all paths that match the insert_xpath.
        '''
        result = TrainingSample(xpath_array=[], post_array=[],
                                url=training_sample.url)
        for xpath, post_array in zip(training_sample.xpath_array,
                                     training_sample.post_array):
            # ensure that we also include the intro :)
            # => add the corresponding tags + is_posts elements with value 0
            if xpath == insert_xpath:
                result.xpath_array.append(xpath)
                result.post_array.append(post_array)
                insert = []
                for element in insert_sequence:
                    insert.append(element)
                    result.xpath_array.append(xpath + insert)
                    result.post_array.append(-1)
            elif insert_xpath == xpath[:len(insert_xpath)]:
                extended_xpath = insert_xpath + insert_sequence + xpath[len(insert_xpath):]
                result.xpath_array.append(extended_xpath)
                result.post_array.append(post_array)
            else:
                result.xpath_array.append(xpath)
                result.post_array.append(post_array)
        return result

    @staticmethod
    def replace_html_tag(training_sample, replace_xpath, replacement_tag):
        '''
        Returns
        -------
        A copy of training_sample where the tag indicated in replace_xpath
        is replaced by replacement_tag.
        '''
        xpath_array = []
        for xpath in training_sample.xpath_array:
            # deep copy of the original xpath to prevent changes to the input data
            xpath = xpath[:]
            if replace_xpath == xpath[:len(replace_xpath)]:
                xpath[len(replace_xpath) - 1] = replacement_tag
            xpath_array.append(xpath)

        return TrainingSample(xpath_array=xpath_array,
                              post_array=training_sample.post_array,
                              url=training_sample.url)

    @staticmethod
    def multiply_subtree(training_sample, duplicate_xpath, copies):
        '''
        duplicate_xpath:
        '''

        logger.debug("mulitply_sequence: duplicate_xpath=%s, copies=%s", duplicate_xpath, copies)

        # 1. parse subtree structure
        is_prefix = True
        is_middle = False
        is_suffix = False

        prefix_training_sample = TrainingSample(xpath_array=[], post_array=[],
                                url=training_sample.url)  # xpaths before the duplicate_xpath
        suffix_training_sample = TrainingSample(xpath_array=[], post_array=[],
                                url=training_sample.url)  # xpaths after duplicate_xpath
        subtrees = []  # subtrees within duplicate_xpaths

        current_subtree = TrainingSample(xpath_array=[], post_array=[],
                                         url=training_sample.url)
        for xpath, post_array in zip(training_sample.xpath_array,
                                     training_sample.post_array):
            if is_prefix:
                if duplicate_xpath == xpath:
                    is_prefix = False
                    is_middle = True
                    current_subtree = TrainingSample(xpath_array=[xpath],
                                                     post_array=[post_array],
                                                     url=training_sample.url)
                else:
                    prefix_training_sample.xpath_array.append(xpath)
                    prefix_training_sample.post_array.append(post_array)

            elif is_middle:
                if duplicate_xpath == xpath:
                    subtrees.append(current_subtree)
                    current_subtree = TrainingSample(xpath_array=[xpath],
                                                     post_array=[post_array],
                                                     url=training_sample.url)
                elif duplicate_xpath == xpath[:len(duplicate_xpath)]:
                    current_subtree.xpath_array.append(xpath)
                    current_subtree.post_array.append(post_array)
                else:
                    if current_subtree:
                        subtrees.append(current_subtree)
                    is_middle = False
                    is_suffix = True

            elif is_suffix:
                suffix_training_sample.xpath_array.append(xpath)
                suffix_training_sample.post_array.append(post_array)

        if not subtrees:
            raise ValueError("Couldn't extract valid subtrees from xpath array.")

        # 2. reassemble subtree
        result_training_sample = prefix_training_sample
        for _ in range(copies):
            selected_subtree = choice(subtrees)
            result_training_sample.xpath_array.extend(selected_subtree.xpath_array)
            result_training_sample.post_array.extend(selected_subtree.post_array)
        result_training_sample.xpath_array.extend(suffix_training_sample.xpath_array)
        result_training_sample.post_array.extend(suffix_training_sample.post_array)

        return result_training_sample

def choose_path(xpath_array, prefix_suffix_len, index=None, unittest=False):
    '''
    Selects a random element from the given xpath array, excluding the begin and start points.

    Returns
    -------
    a tuple (xpath, prefix, suffix)
    '''
    for _ in range(5):
        if not unittest:
            index = randint(1, len(xpath_array) - 2)
        xpath_array_with_sep = [ xpath+["[SEP]"] for xpath in xpath_array]
        # print([x for x in chain(*xpath_array_with_sep[:index + 1])])
        prefix = [x for x in chain(*xpath_array_with_sep[:index + 1])][(-prefix_suffix_len - 2):-2]
        suffix = ['[SEP]'] + [x for x in islice(chain(*xpath_array_with_sep[index + 1:]), prefix_suffix_len - 1)]
        if len(suffix) == prefix_suffix_len and len(prefix) == prefix_suffix_len:
            return xpath_array[index], prefix, suffix
        if not unittest:
            index = None

    # return as is
    logger.warning("Cannot determine any xpath with a sufficiently long prefix and suffix.")
    print("RETURN::::::NONE")
    return None, None, None


class FeatureManipulations(Enum):
    '''
    An Enum of all available feature manipulations
    '''
    REPLACE_TAG = 1
    INSERT_SEQUENCE = 2
    MULTIPLY_SEQUENCE = 3  # should be performed first and only once



# number of manipulations to apply for modifying the feature array
MIN_MANIPULATIONS = 1
MAX_MANIPULATIONS = 4
DEFAULT_AUGMENTATION_COUNT = 7

class TrainingSampleAugmenter(object):

    def __init__(self, training_sample, language_model, xpath_cleanup, min_manipulations=MIN_MANIPULATIONS, max_manipulations=MAX_MANIPULATIONS, augmentation_count=DEFAULT_AUGMENTATION_COUNT):
        self.training_sample = training_sample
        self.augmentation_count = augmentation_count
        self.min_manipulations = MIN_MANIPULATIONS
        self.max_manipulations = MAX_MANIPULATIONS
        self.language_model = language_model
        self.xpath_cleanup = xpath_cleanup

    def __iter__(self):
        return self

    def __next__(self):
        if self.augmentation_count <= 0:
            raise StopIteration
        self.augmentation_count -= 1

        if self.augmentation_count == 0:
            logger.debug("original training sample: %s", pp.pformat(self.training_sample.xpath_array))
            return self.training_sample
        else:
            return self.get_augmented_training_sample()

    def get_augmented_training_sample(self):
        '''
        Returns
        -------
        A randomly augmented version of the original feature_array

        Note
        ----
        - the number of manipulations is determined randomly based on
          MIN_MANIPULATIONS and MAX_MANIPULATIONS.
        - the MULTIPLY_SEQUENCE manipulation is only applied once.
        '''
        num_manipulations = randint(self.min_manipulations, self.max_manipulations)
        manipulations = []

        # obtain the sequence of manipulations to apply
        while len(manipulations) < num_manipulations:
            next_manipulation = choice(list(FeatureManipulations))
            if next_manipulation == FeatureManipulations.MULTIPLY_SEQUENCE:
                # multiply should only be applied once
                if FeatureManipulations.MULTIPLY_SEQUENCE in manipulations:
                    continue
                manipulations.insert(0, next_manipulation)
            else:
                manipulations.append(next_manipulation)

        logger.debug("augmentation counter: %s, manipulations: %s", self.augmentation_count, manipulations)

        # create a augmented training sample based on the initial training sample
        augmented_training_sample = TrainingSample(xpath_array=self.training_sample.xpath_array, post_array=self.training_sample.post_array, url=self.training_sample.url)

        for manipulation in manipulations:
            if manipulation == FeatureManipulations.REPLACE_TAG:
                augmented_training_sample = FeatureArray.replace_tag(augmented_training_sample, self.language_model)

            elif manipulation == FeatureManipulations.INSERT_SEQUENCE:
                augmented_training_sample = FeatureArray.insert_sequence(augmented_training_sample, self.language_model)

            elif manipulations == FeatureManipulations.MULTIPLY_SEQUENCE:
                duplicate_xpath = FeatureArray.get_first_post_xpath(augmented_training_sample)
                augmented_training_sample = FeatureArray.multiply_subtree(augmented_training_sample,
                                                                  duplicate_xpath, copies=randint(1, 3))

        # run the cleanup
        cleaned_xpath = self.xpath_cleanup.clean_xpath_with_valid_tags(augmented_training_sample.xpath_array)

        augmented_training_sample = TrainingSample(xpath_array=cleaned_xpath, post_array=augmented_training_sample.post_array, url=augmented_training_sample.url)

        logger.debug("augmented training sample: %s", pp.pformat(augmented_training_sample.xpath_array))
        logger.debug("---------------------------------------------------")
        # self.training_sample = augmented_training_sample
        return augmented_training_sample


class XpathCleanup(object):

    SEPARATOR_ELEMENT_NAME = '[SEP]'

    def __init__(self, valid_html_tag_list):
        self.valid_html_tag_list = valid_html_tag_list

    def clean_xpath_with_valid_tags(self, xpath_array):
        cleaned_xpath_array = []
        for xpath in xpath_array:
            cleaned_xpath_array.append(
                list(filter(lambda tag: tag in self.valid_html_tag_list and tag != SEPARATOR_ELEMENT_NAME, xpath)))
        return cleaned_xpath_array

    def clean_xpath_with_valid_tags_for_embedding(self, xpath_array, html_embedding_tool):
        cleaned_xpath_array = []
        for xpath in xpath_array:
            cleaned_xpath_array.append(
                list(filter(lambda tag: tag in html_embedding_tool.embedding, xpath)))
        return cleaned_xpath_array

# default values for max page size, tree length
DEFAULT_MAX_PAGE_SIZE = 700
DEFAULT_MAX_TREE_LEN = 100

class FeatureTensor(object):

    def __init__(self, training_sample, tensor, max_page_size=DEFAULT_MAX_PAGE_SIZE, max_tree_len=DEFAULT_MAX_TREE_LEN):
        self.training_sample = training_sample
        self.max_page_size = max_page_size
        self.max_tree_len = max_tree_len
        self.tensor = tensor
        self.normalize_post_array()

    def normalize_post_array(self):
        normalized_post_array = FeatureTensor.normalize_post_array_static(self.training_sample.post_array, self.max_page_size)
        self.training_sample =  TrainingSample(xpath_array=self.training_sample.xpath_array, post_array=normalized_post_array, url=self.training_sample.url)

    @staticmethod
    def normalize_post_array_static(post_array, max_page_size):
        if len(post_array) < max_page_size:
            # # append -1 to fill up to max_page_size
            for _ in range(len(post_array), max_page_size):
                post_array.append(-1)
        return post_array[:max_page_size]


    @staticmethod
    def get_feature_tensor(
            training_sample,
            html_embedding_tool,
            xpath_cleanup,
            max_page_size=DEFAULT_MAX_PAGE_SIZE,
            max_tree_len=DEFAULT_MAX_TREE_LEN):

        """
        Returns
        -------
        A FeatureTensor containing (i) the feature_array and (ii) the corresponding
        three dimensional tensor.
        """
        tensor_result = []
        empty_line = max_tree_len * [[0.] * len(html_embedding_tool)]

        # clean the xpath array first
        cleaned_xpath_array  = xpath_cleanup.clean_xpath_with_valid_tags_for_embedding(training_sample.xpath_array, html_embedding_tool)

        try:
            for xpath in chain(cleaned_xpath_array,
                               repeat([], max_page_size - len(cleaned_xpath_array))):
                xpath = xpath[:max_tree_len]

                # calculate the tensors
                features = [html_embedding_tool[tag] for tag in xpath] + empty_line[:(
                        max_tree_len - len(xpath))]
                tensor_result.append(features)
        except TypeError:
            logger.error("Type Error: %s", xpath)

        if len(tensor_result) > max_page_size:
            logger.debug("Document exceeds max. page size of %d (%d) - truncated." % (max_page_size, len(tensor_result)))
            tensor_result = tensor_result[:max_page_size]

        # fill the rest of the spots with empty embeddings to ensure tenors
        # of equal size.
        # print(len(xpath_array), len(result))
        # for xpath in range(max_page_size - len(training_sample.xpath_array)):
        #    tensor_result.append(empty_line)

        result_training_sample = TrainingSample(
                        xpath_array=cleaned_xpath_array,
                        post_array=FeatureTensor.normalize_post_array_static(training_sample.post_array, max_page_size),
                        url=training_sample.url)

        return FeatureTensor(training_sample=result_training_sample, tensor=tensor_result, max_page_size=max_page_size, max_tree_len=max_tree_len)













