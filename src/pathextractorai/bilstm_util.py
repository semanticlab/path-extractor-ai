#!/usr/bin/env python3

'''
Helper for obtaining results from the biLSTM.
'''

import gzip
import os
from csv import reader
from itertools import chain
from random import uniform

import numpy as np

from keras.models import Model, model_from_json

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
VOCABULARY = SCRIPT_DIR + '/html_vocabulary.cvs.gz'
MODEL_NAME = SCRIPT_DIR + '/model-g{}'


TAG_MASK = '[MASK]'
TAG_UNKNOWN = '[UNKNOWN]'


class LanguageModel(object):

    def __init__(self, model_name=MODEL_NAME, vocabulary=VOCABULARY, gap_sizes=[1, 2, 3]):
        self.vocabulary, self.rev_vocabulary = self.read_vocabulary_file(vocabulary)
        self.unknown_tag_index = self.vocabulary[TAG_UNKNOWN]
        self.model = {}
        self.gap_sizes = gap_sizes
        for gap_size in gap_sizes:
            with open(MODEL_NAME.format(gap_size) + '.json') as f:
                self.model[gap_size] = model_from_json(f.read())
                self.model[gap_size].load_weights(MODEL_NAME.format(gap_size) + '.h5')

    @staticmethod
    def read_vocabulary_file(fname):
        with gzip.open(fname, 'rt') as f:
            csv = reader(f)
            vocabulary = {word: int(idx) for word, idx in csv}
            rev_vocabulary = {idx: word for word, idx in vocabulary.items()}
            return vocabulary, rev_vocabulary

        raise "Cannot read vocabulary file!"

    def get_vocabulary_idx_sequence(self, prefix, suffix, gap_size):
        '''
        Returns: 
        --------
        a list of indices encoding the given sequence using the vocabulary
        provided to the class.
        '''
        return [self.vocabulary.get(term, self.unknown_tag_index)
                for term in chain(prefix, gap_size * [TAG_MASK], suffix)]

    def estimate(self, prefix, suffix, gap_size):
        '''
        Provides an estimation of the tags used within the gap.
        '''
        result = []
        x = np.asarray([self.get_vocabulary_idx_sequence(prefix, suffix, gap_size)])
        y = self.model[gap_size].predict(x)[0]

        tag = self.rev_vocabulary[self.get_result_index(y[len(prefix)])]
        result.append(tag)
        if gap_size != 1:
            prefix = prefix[1:] + [tag]
            result.extend(self.estimate(prefix, suffix, gap_size - 1))

        return result

    @staticmethod
    def get_result_index(v, stop_value=None):
        '''
        Returns:
        --------
        The index of the result based on the results probability.

        Note:
        -----
        Use np.argmax, if you are only interested in obtaining the most likely result.
        '''
        # required for unit testing
        if not stop_value:
            stop_value = uniform(0, sum(v))
        s = 0.
        for idx, value in enumerate(v):
            s += value
            if s >= stop_value:
                return idx
        return idx


def test_lm():
    l = LanguageModel()
    prefix = ['html', 'body', 'span', 'span', 'ul']
    suffix = ['b', '[SEP]', 'html', 'body', 'span']
    print(l.estimate(prefix, suffix, 1))
    print(l.estimate(prefix, suffix, 2))
    print(l.estimate(prefix, suffix, 3))


def test_get_result_index():
    v = [0.01, 0.1, 0.9, 0.5, 0.01]
    assert LanguageModel.get_result_index(v, 0.001) == 0
    assert LanguageModel.get_result_index(v, 0.11) == 1
    assert LanguageModel.get_result_index(v, 1.51) == 3
    assert LanguageModel.get_result_index(v, 1.52) == 4
    assert LanguageModel.get_result_index(v, 99.0) == 4
