#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 09.8. 2019

.. codeauthor: Norman Süsstrunk <norman.suesstrunk@htwchur.ch>
'''

import os
import pickle
import pprint
from json import load
from pathlib import Path

import numpy as np
import yaml

from pathextractorai.augment_features import DEFAULT_AUGMENTATION_COUNT, TrainingSampleAugmenter, \
    XpathCleanup, FeatureTensor, DEFAULT_MAX_PAGE_SIZE, DEFAULT_MAX_TREE_LEN, FeatureArray
from pathextractorai.bilstm_util import LanguageModel
from pathextractorai.embedding_util import load_valid_html_tags, HtmlEmbeddingTool
from pathextractorai.tree_util import get_dom_tree, get_tree_features

pp = pprint.PrettyPrinter(indent=4, width=400)

from pathextractorai.logging import get_logger
logger = get_logger(__name__)

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
FORUM_DATA_DIR = SCRIPT_DIR + '/../../forum_data/train'

EVA_DATA_DIR = SCRIPT_DIR + '/../../forum_data/evaluation/'

FORUM_XPATH_CONFIG_DIR = SCRIPT_DIR + '/forum_xpath_configs/'

PP_PRESS_KEY = "ppPress"
PHP_BB_KEY = "phpBB"

DEFAULT_POSTS_DUMP_PICKLE_FILENAME = "posts_feature_tensors.pickle"

DEFAULT_XPATH_YAML_CONFIGS = {
    PP_PRESS_KEY: "bbPress_pathconfig.yaml",
    PHP_BB_KEY: "phpBB_pathconfig.yaml"
}

VALID_HTML_TAGS = load_valid_html_tags()
XPATH_CLEANUP = XpathCleanup(VALID_HTML_TAGS)
HTML_EMBEDDING_TOOL = HtmlEmbeddingTool()

class CorpusUtil(object):

    def __init__(self,
                 forum_data_dir=FORUM_DATA_DIR,
                 xpaths_yaml_config_dict=DEFAULT_XPATH_YAML_CONFIGS,
                 augmentation_count=DEFAULT_AUGMENTATION_COUNT
                 ):
        self.forum_data_dir = forum_data_dir
        self.xpaths_yaml_config = CorpusUtil.load_path_yaml_configs(xpaths_yaml_config_dict)
        self.forum_file_counter = {PP_PRESS_KEY: 0, PHP_BB_KEY: 0}
        self.language_model = LanguageModel()
        self.valid_html_tags = load_valid_html_tags()
        self.augmentation_count = augmentation_count

    def read_forum_data_to_augmented_training_samples(self):
        training_samples = []
        for fname in Path(self.forum_data_dir).glob('*.json'):
            with open(fname.absolute()) as src:
                logger.debug("reading file: %s", src.name)
                json = load(src)
                html = json['html']
                url = json['url']
                dom_tree = get_dom_tree(html);
                # iterate all configs for the forums
                for forum_key, xpath_yaml_config in self.xpaths_yaml_config.items():

                    # check if the page is a forum page
                    if len(dom_tree.xpath(xpath_yaml_config.match_path)) > 0:
                        logger.debug('forum page readed: %s, file %s ', forum_key, fname.absolute())
                        with open(fname.absolute()) as src:
                            html = load(src)['html']
                            # generate the dom tree
                            dom_tree = get_dom_tree(html)
                            # read in the training sample that constructs the xpath and the output vector
                            training_sample = get_tree_features(dom_tree=dom_tree, url=url,
                                                                post_path_config=xpath_yaml_config)

                            # check if the site really contains some posts
                            if FeatureArray.counted_post_xpaths(training_sample.xpath_array, training_sample.post_array):

                                # augment the training samples
                                training_sample_augmenter = TrainingSampleAugmenter(
                                    training_sample=training_sample,
                                    language_model = self.language_model,
                                    xpath_cleanup = XPATH_CLEANUP,
                                    augmentation_count=self.augmentation_count)

                                for augmented_training_sample in training_sample_augmenter:
                                    training_samples.append(augmented_training_sample)
                            else:
                                logger.debug("not forum posts found in file %s", src.name)
        return training_samples

    def load_domtree_from_json(self, filename):
        test_file_json = Path(self.forum_data_dir + filename)
        with open(test_file_json.absolute()) as src:
            json = load(src)
            html = json['html']
            return get_dom_tree(html);


    def read_evaluation_samples(self):
        forum_eval_json_file = EVA_DATA_DIR+"forum-evaluation.json"
        with open(forum_eval_json_file) as src:
            eval_json_datas = load(src)
            for eval_json_data in eval_json_datas:
                print(EVA_DATA_DIR+eval_json_data['file'])
                forum_html_content = open(EVA_DATA_DIR+eval_json_data['file']).read()
                # print(forum_html_content)
                dom_tree = get_dom_tree(forum_html_content);
                eval_json_data['dom_tree'] = dom_tree
            return eval_json_datas



    @staticmethod
    def generate_feature_tensors(training_samples, max_page_size=DEFAULT_MAX_PAGE_SIZE, max_tree_len=DEFAULT_MAX_TREE_LEN):
        feature_tensors = {'x': [], 'y': []}
        for training_sample in training_samples:
            feature_tensor = FeatureTensor.get_feature_tensor(
                training_sample=training_sample,
                html_embedding_tool=HTML_EMBEDDING_TOOL,
                max_page_size=max_page_size,
                xpath_cleanup=XPATH_CLEANUP,
                max_tree_len=max_tree_len)
            feature_tensors['x'].append(feature_tensor.tensor)
            feature_tensors['y'].append(feature_tensor.training_sample.post_array)
        numpy_x = np.array(feature_tensors['x'])
        numpy_y = np.array(feature_tensors['y'])
        return {'x': numpy_x, 'y': numpy_y,
                'training_samples': training_samples}

    @staticmethod
    def pickle_dump_feature_tensors(feature_tensors,
                                     dump_file_name=DEFAULT_POSTS_DUMP_PICKLE_FILENAME):
        dump_file = open(SCRIPT_DIR + "/../../" + dump_file_name, 'wb')
        pickle.dump(feature_tensors, dump_file, protocol=4)
        dump_file.close()
        logger.info("dumped feature tensors to  %s", dump_file)

    @staticmethod
    def pickle_load_feature_tensors(dump_file_name=DEFAULT_POSTS_DUMP_PICKLE_FILENAME):
        dump_file = open(SCRIPT_DIR + "/../../" + dump_file_name, 'rb')
        feature_tensors = pickle.load(dump_file)
        dump_file.close()
        logger.info("loaded feature tensors from %s", dump_file)
        return feature_tensors

    @staticmethod
    def load_path_yaml_configs(xpaths_yaml_config_dict):
        xpaths_yaml_config = {}
        for forum_xpath_key, forum_configfile_value in xpaths_yaml_config_dict.items():
            xpaths_yaml_config.update(
                {forum_xpath_key: CorpusUtil.load_path_yaml_config(forum_configfile_value)})
        return xpaths_yaml_config

    @staticmethod
    def load_path_yaml_config(path_config_yaml_file):
        # logger.debug('reading from file: %s', path_config_yaml_file)
        with open(FORUM_XPATH_CONFIG_DIR + path_config_yaml_file, 'r') as yaml_stream:
            try:
                return PathConfig(yaml.safe_load(yaml_stream))
            except yaml.YAMLError as exc:
                logger.warn(exc)


    @staticmethod
    def get_max_pagesize_treelength(training_samples):
        # calculate the max_page_size / max_tree_length
        max_page_size = 0  # number of xpaths
        max_page_size_sample = 0

        max_tree_length = 0  # max length of all xpaths
        max_tree_length_sample = 0

        max_page_size_sum = 0

        for training_sample in training_samples:
            sample_page_size = len(training_sample.xpath_array)
            max_page_size_sum = max_page_size_sum + sample_page_size
            # logger.info("augmented training sample page size: %s", sample_page_size)
            sample_max_tree_length = 0
            for xpath in training_sample.xpath_array:
                tree_length = len(xpath)
                if tree_length >= sample_max_tree_length:
                    sample_max_tree_length = tree_length

            if sample_page_size > max_page_size:
                max_page_size = sample_page_size
                max_page_size_sample = training_sample

            if sample_max_tree_length >= max_tree_length:
                max_tree_length = sample_max_tree_length
                max_tree_length_sample = training_sample
        return max_page_size, max_tree_length, max_page_size_sample, max_tree_length_sample, max_page_size_sum / len(training_samples)


class PathConfig():
    CONFIG_KEYS = ['post_list', 'post_username', 'post_content', 'post_url',
                   'post_date', 'match_path']

    def __init__(self, yaml_dict):
        self.yaml_dict = yaml_dict
        if not self.validate():
            logger.error("yaml path config not valid: %s ",
                         pp.pformat(self.yaml_dict))

    def validate(self):
        for config_key in self.CONFIG_KEYS:
            if not self.validate_config_key(config_key):
                return False
        return True

    def validate_config_key(self, config_key):
        if not config_key in self.yaml_dict.keys():
            logger.warn('no path for config_key defined: %s, ', config_key)
            return False
        else:
            # set as class attribute
            setattr(self, config_key, self.yaml_dict[config_key])
            return True


