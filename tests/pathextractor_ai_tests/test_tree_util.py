#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Jun 7, 2019

.. codeauthor: Albert Weichselbraun <albert@weichselbraun.net>
'''

import unittest

from pathextractorai.augment_features import FeatureArray
from pathextractorai.bilstm_util import LanguageModel
from pathextractorai.corpus_util import CorpusUtil, PP_PRESS_KEY
from pathextractorai.embedding_util import load_valid_html_tags
from pathextractorai.tree_util import get_dom_tree, normalize_dom_text, \
    get_tree_features, \
    get_post_content, clean_xpath, POSTS_CONTENT_DICT_KEY, XPATH_DICT_KEY, cleanup_trailing_redundant_tags, \
    is_narrow_match, compare_xpath_lists, is_broader_match, is_mismatch, compare_xpath_lists
from pathextractorai.logging import logging as logger

post_path_config = CorpusUtil.load_path_yaml_config('bbPress_pathconfig.yaml')

VALID_HTML_TAGS = load_valid_html_tags()


class TestRandomTreeManipulator(unittest.TestCase):

    def test_normalize_dom_text(self):
        assert normalize_dom_text("") == ""

        assert normalize_dom_text("12345") == "*"
        assert normalize_dom_text("123456") == "**"

        # correct handling of hmtl entities
        assert normalize_dom_text("&nbsp;") == ""
        assert normalize_dom_text("1234&uuml;") == "*"

    def test_get_tree_features(self):
        example_html = '<html><body><div><ul><li>list1</li><li>list2</li></ul><ul><li>list3</li><li>list4</li></ul></div></body><html>'
        dom_tree = get_dom_tree(example_html)
        assert get_tree_features(dom_tree, 'test.com/test.html',
                                 post_path_config).xpath_array == [['html'],
                                                                   ['html', 'body'],
                                                                   ['html', 'body',
                                                                    'div'],
                                                                   ['html', 'body',
                                                                    'div', 'ul'],
                                                                   ['html', 'body',
                                                                    'div', 'ul',
                                                                    'li',
                                                                    '*'],
                                                                   ['html', 'body',
                                                                    'div', 'ul',
                                                                    'li',
                                                                    '*'],
                                                                   ['html', 'body',
                                                                    'div', 'ul'],
                                                                   ['html', 'body',
                                                                    'div', 'ul',
                                                                    'li',
                                                                    '*'],
                                                                   ['html', 'body',
                                                                    'div', 'ul',
                                                                    'li',
                                                                    '*']]

    # TODO: fix this test
    @unittest.skip
    def test_get_post_content(self):
        url = 'https://forum.franceparkinson.fr/forums/topic/et-vous-quels-sports-faites-vous/'
        corpus_util = CorpusUtil()

        # test with a bb press forum site
        dom_tree = corpus_util.load_domtree_from_json(
            filename='https%3A%2F%2Fforum.franceparkinson.fr%2Fforums%2Ftopic%2Fet-vous-quels-sports-faites-vous%2F.json')
        feature_array = FeatureArray(
            training_sample=get_tree_features(dom_tree=dom_tree, url=url,
                                              post_path_config=
                                              corpus_util.xpaths_yaml_config[
                                                  PP_PRESS_KEY]),
            html_embbedding=EMBEDDING,
            language_model=LanguageModel(),
            valid_html_tag_list=VALID_HTML_TAGS
        );

        for xpath in feature_array.counted_post_xpaths(
                feature_array.training_sample.post_array):
            xpath = clean_xpath(xpath)
            posts_content = get_post_content(dom_tree, xpath)
            if posts_content:
                for post_content in posts_content[POSTS_CONTENT_DICT_KEY]:
                    assert post_content
                    logger.info(
                        '\n----------------------------------\nxpath: %s\npost content: \n%s\n----------------------------------\n\n',
                        posts_content[XPATH_DICT_KEY], post_content)


    def test_remove_trailing_tags(self):
        xpath = ['div','p','br']
        xpath = cleanup_trailing_redundant_tags(xpath)
        assert xpath == ['div']

        xpath = ['div', 'p', 'br', 'a']
        xpath = cleanup_trailing_redundant_tags(xpath)
        assert xpath == ['div']

        xpath = ['div', 'br', 'div', 'p', 'br', 'a']
        xpath = cleanup_trailing_redundant_tags(xpath)
        assert xpath == ['div', 'br', 'div']


    def test_is_narrower_match(self):
        xpath_ref = [1, 1, 3, 4]
        xpath_compare = [1, 1, 3]
        mismatch, missing, added = compare_xpath_lists(xpath_ref, xpath_compare)
        narrow_match, diff = is_narrow_match(missing, added)
        assert narrow_match
        assert diff == [4]

    def test_is_broader_match(self):
        xpath_ref = [1, 1, 3, 4]
        xpath_compare = [1, 1, 3, 4, 5]
        mismatch, missing, added = compare_xpath_lists(xpath_ref, xpath_compare)
        narrow_match, added = is_broader_match(missing, added)
        assert narrow_match
        assert added == [5]

    def test_compare_xpath_lists_rev(self):
        xpath_ref = [1, 1, 1, 2, 3]
        xpath_compare = [1, 1, 1]
        mismatch, missing, added = compare_xpath_lists(xpath_ref, xpath_compare)
        assert mismatch == False
        assert missing == [2, 3]
        assert added == []

        xpath_ref = [1, 1, 1]
        xpath_compare = [1, 1, 1, 2, 3]
        mismatch, missing, added = compare_xpath_lists(xpath_ref, xpath_compare)
        assert mismatch == False
        assert added == [2, 3]
        assert missing == []

        xpath_ref = [1, 1, 1]
        xpath_compare = [1, 1, 1]
        mismatch, missing, added = compare_xpath_lists(xpath_ref, xpath_compare)
        assert mismatch == False
        assert added == []
        assert missing == []

        xpath_ref = [1, 1, 2]
        xpath_compare = [1, 1, 3]
        mismatch, missing, added = compare_xpath_lists(xpath_ref, xpath_compare)
        assert mismatch == True
        assert added == []
        assert missing == []

    def test_is_mismatch(self):
        xpath_ref = [1, 2, 3]
        xpath_compare = [3, 4]
        missing, added = compare_xpath_lists(xpath_ref, xpath_compare)
        mismatch = is_mismatch(missing, added)
        assert mismatch
        logger.info("missing: %s, added: %s", missing, added)

if __name__ == '__main__':
    unittest.main()
