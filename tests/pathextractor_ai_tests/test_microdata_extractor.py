#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on Jun 7, 2019

.. codeauthor: Norman Süsstrunk <norman.suesstrunk@htwchur.ch>
'''
import pprint
import unittest

from pathextractorai.logging import get_logger
from pathextractorai.microdata_extractor import MicroDataExtractor, \
    TEST_FORUM_DATA_MICRODATA_URL

pp = pprint.PrettyPrinter(indent=2)
logger = get_logger(__name__)


class TestMicroDataExtractor(unittest.TestCase):

    def test_microdata_extractor(self):
        for url in TEST_FORUM_DATA_MICRODATA_URL:
            microdata_extractor = MicroDataExtractor(url)
            posts = microdata_extractor.extract_posts()
            for post in posts:
                assert post
                # logger.info('extracted post (from microdata) from url=%s: %s', url, pp.pformat(post))


    def test_microdata_extractor_postcontent_to_xpath(self):
        microdata_extractor = MicroDataExtractor(TEST_FORUM_DATA_MICRODATA_URL[0])

        # extract the content for every html path
        element_content_dict = microdata_extractor.extractXpathContents()
        #logger.info("element_content_dict: %s", element_content_dict)

        # calculate the xpaths for the microdata posts
        post_xpaths = microdata_extractor.getXpathsForMicrodataPosts()

        assert len(post_xpaths) > 0
        # logger.info("microdata post xpaths: %s", post_xpaths)


if __name__ == '__main__':
    unittest.main()
