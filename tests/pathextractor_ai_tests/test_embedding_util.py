import unittest
import numpy as np
from pathextractorai.embedding_util import load_valid_html_tags, HtmlEmbeddingTool

class TestEmbeddingUtil(unittest.TestCase):

    def test_load_valid_html_tags(self):
        valid_html_tags = load_valid_html_tags()
        assert len(valid_html_tags) > 0

    def test_html_embedding(self):
        html_embedding_tool = HtmlEmbeddingTool()
        html_embedding_np =  np.array(html_embedding_tool['div'])
        print(html_embedding_np.shape) # (20,)

    def test_html_embedding(self):
        html_embedding_tool = HtmlEmbeddingTool()
        author_tag = "author"
        if author_tag in html_embedding_tool.embedding:
            html_embedding = html_embedding_tool['author']
            assert html_embedding


if __name__ == '__main__':
    unittest.main()
