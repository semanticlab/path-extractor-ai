#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 09.8. 2019

.. codeauthor: Norman Süsstrunk <norman.suesstrunk@htwchur.ch>
'''
import unittest
import os
from pathextractorai.augment_features import TrainingSample, FeatureArray, XpathCleanup, \
    TrainingSampleAugmenter
from pathextractorai.bilstm_util import LanguageModel
from pathextractorai.corpus_util import CorpusUtil
from pathextractorai.embedding_util import load_valid_html_tags, HtmlEmbeddingTool
from pathextractorai.logging import get_logger
from pathextractorai.pathextractor_keras_model import PathExtractorKerasModel, trainAndEvaluateModel

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
BISTLM_LANGUAGE_MODEL = LanguageModel()
VALID_HTML_TAGS = load_valid_html_tags()
XPATH_CLEANUP = XpathCleanup(VALID_HTML_TAGS)
HTML_EMBEDDING = HtmlEmbeddingTool()
logger = get_logger(__name__)


class TestModelTraining(unittest.TestCase):

    @staticmethod
    def get_training_sample():
        xpath_array = [
            ['html'],
            ['html', 'body'],
            ['html', 'body', 'div'],
            ['html', 'body', 'div', 'ul'],
            ['html', 'body', 'div', 'ul', 'li', '*'],
            ['html', 'body', 'div', 'ul', 'li', '*'],
            ['html', 'body', 'div', 'ul'],
            ['html', 'body', 'div', 'ul', 'li', '*'],
            ['html', 'body', 'div', 'ul', 'li', '*']
        ]
        # denotes witch elements in the xpath_array are actual paths to posts
        post_array = [-1, -1, -1, -1, 1, 1, -1, 1, 1]
        return TrainingSample(xpath_array=xpath_array, post_array=post_array,
                              url='http://test.com')


    def test_predict_most_common_xpath(self):
        xpath_array = [
            ['1', '2'],
            ['1', '2', '3'],
            ['1', '2', '3', '4'],
            ['1', '2', '3', '4'],
            ['1', '2', '3', '4'],
            ['1', '2', '3'],
            ['1', '2']
        ]
        post_array = [-1, 1, 1, 1, 1, 1, -1]
        xpath_most_common = PathExtractorKerasModel.predict_unique_xpath(xpath_array, post_array)
        assert  xpath_most_common == "/1/2/3/4"

        # test with no post xpaths at all
        post_array = [-1, -1, -1, -1, -1, -1, -1]
        xpath_most_common = PathExtractorKerasModel.predict_unique_xpath(xpath_array, post_array)
        assert xpath_most_common == None

    @unittest.skip("takes a long time to run, disabled by default")
    def test_model_with_test_data(self):

        max_page_size = 30
        max_tree_length = 10
        augmentation_count = 5
        epochs = 5

        training_sample_augmenter = TrainingSampleAugmenter(
                                                            TestModelTraining.get_training_sample(),
                                                            BISTLM_LANGUAGE_MODEL,
                                                            xpath_cleanup=XPATH_CLEANUP,
                                                            augmentation_count=augmentation_count)

        training_samples = []
        for augmented_training_sample in training_sample_augmenter:
            training_samples.append(augmented_training_sample)

        # generate the tensors
        feature_tensors = CorpusUtil.generate_feature_tensors(
            training_samples=training_samples,
            max_page_size=max_page_size,
            max_tree_len=max_tree_length)

        trainAndEvaluateModel(
            feature_tensors=feature_tensors, max_page_size=max_page_size, max_tree_length=max_tree_length, epochs=epochs)

if __name__ == '__main__':
    unittest.main()
