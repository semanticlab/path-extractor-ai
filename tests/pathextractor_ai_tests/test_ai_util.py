#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 09.8. 2019

.. codeauthor: Norman Süsstrunk <norman.suesstrunk@htwchur.ch>
'''
import pprint
import unittest
import numpy as np
from pathextractorai.ai_util import normalize_predicted_postarray, \
    compare_post_arrays, EvaluationItem
from pathextractorai.logging import get_logger

pp = pprint.PrettyPrinter(indent=2)
logger = get_logger(__name__)

POST_ARRAY_UNNORMALIZED = np.array([0, 1.1, 0, 2.3, -1])
POST_ARRAY_NORMALIZED = np.array([-1, 1, -1, 1, 1])
POST_ARRAY_SIMILAR = np.array([-1, -1, -1, 1, 1])


def get_evaluation_item():
    post_array_gold = np.array([1, -1, -1, 1])
    post_array_compare = np.array([1, 1, -1, -1])
    return EvaluationItem.get_evaluation_item_for_post_arrays(post_array_gold, post_array_compare)

class TestAiUtil(unittest.TestCase):

    def test_normalizing_predicted_post_array(self):
        logger.info('array before normalizing %s', POST_ARRAY_UNNORMALIZED)
        numpy_post_array_normalized = normalize_predicted_postarray(
            POST_ARRAY_UNNORMALIZED)
        logger.info('array after normalizing: %s', numpy_post_array_normalized)
        assert np.array_equal(numpy_post_array_normalized, POST_ARRAY_NORMALIZED)

    def test_post_array_comparison(self):
        assert compare_post_arrays(POST_ARRAY_NORMALIZED, POST_ARRAY_SIMILAR) == 0.8


    def test_evaluation_item(self):

        eval_item = get_evaluation_item()

        assert eval_item.tp == 1
        assert eval_item.fp == 1
        assert eval_item.tn == 1
        assert eval_item.fn == 1
        assert eval_item.precision == 0.5
        assert eval_item.recall == 0.5
        assert eval_item.f1 == 0.5

        # test extrem cases

        eval_item_all_fp = EvaluationItem.get_evaluation_item_for_post_arrays(np.array([-1, -1, -1, -1]), np.array([1, 1, 1, 1]))
        assert eval_item_all_fp.fp == 4

        eval_item_all_fn = EvaluationItem.get_evaluation_item_for_post_arrays(np.array([1, 1, 1, 1]), np.array([-1, -1, -1, -1]))
        assert eval_item_all_fn.fn == 4

    def test_mean_eval_calculation(self):
        eval_items = []
        eval_item_1 = get_evaluation_item()
        eval_items.append(eval_item_1)
        eval_item_2 = get_evaluation_item()
        eval_items.append(eval_item_2)
        mean_eval_item = EvaluationItem.mean_eval_from_eval_items(eval_items)
        assert mean_eval_item.fn == eval_item_1.fn
        assert mean_eval_item.precision == eval_item_1.precision

if __name__ == '__main__':
    unittest.main()