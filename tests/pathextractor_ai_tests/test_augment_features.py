#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Jun 7, 2019

.. codeauthor: Norman Suesstrunk <norman.suesstrunk@htwchur.ch>
"""
import unittest
import pprint
import os
from itertools import chain, repeat

from pathextractorai.bilstm_util import LanguageModel
from pathextractorai.augment_features import FeatureArray, TrainingSample, choose_path, TrainingSampleAugmenter, \
    XpathCleanup, FeatureTensor
from pathextractorai.embedding_util import load_valid_html_tags, HtmlEmbeddingTool
import numpy as np
from pathextractorai.logging import get_logger

logger = get_logger(__name__)
pp = pprint.PrettyPrinter(indent=4,width=200)

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
BISTLM_LANGUAGE_MODEL = LanguageModel()
VALID_HTML_TAGS = load_valid_html_tags()
XPATH_CLEANUP = XpathCleanup(VALID_HTML_TAGS)
HTML_EMBEDDING_TOOL = HtmlEmbeddingTool()

def get_training_sample():
    xpath_array = [
        ['html'],
        ['html', 'body'],
        ['html', 'body', 'div'],
        ['html', 'body', 'div', 'ul'],
        ['html', 'body', 'div', 'ul', 'li', '*'],
        ['html', 'body', 'div', 'ul', 'li', '*'],
        ['html', 'body', 'div', 'ul'],
        ['html', 'body', 'div', 'ul', 'li', '*'],
        ['html', 'body', 'div', 'ul', 'li', '*']
    ]
    # denotes witch elements in the xpath_array are actual paths to posts
    post_array = [-1, -1, -1, -1, 1, 1, -1, 1, 1]

    return TrainingSample(xpath_array=xpath_array,
                          post_array=post_array,
                          url='http://test.com/test.html')


class TestAugmentFeatures(unittest.TestCase):

    def test_insert_tag_sequence(self):
        result = FeatureArray.insert_tag_sequence(get_training_sample(),
                                                  ['html', 'body', 'div'],
                                                  ['span', 'div', 'span'])
        assert len(result.xpath_array) == len(result.post_array)
        assert result.xpath_array == [['html'],
                                      ['html', 'body'],
                                      ['html', 'body', 'div'],
                                      ['html', 'body', 'div', 'span'],
                                      ['html', 'body', 'div', 'span', 'div'],
                                      ['html', 'body', 'div', 'span', 'div', 'span'],
                                      ['html', 'body', 'div', 'span', 'div', 'span', 'ul'],
                                      ['html', 'body', 'div', 'span', 'div', 'span', 'ul', 'li', '*'],
                                      ['html', 'body', 'div', 'span', 'div', 'span', 'ul', 'li', '*'],
                                      ['html', 'body', 'div', 'span', 'div', 'span', 'ul'],
                                      ['html', 'body', 'div', 'span', 'div', 'span', 'ul', 'li', '*'],
                                      ['html', 'body', 'div', 'span', 'div', 'span', 'ul', 'li', '*']]
        assert result.post_array == [-1, -1, -1, -1, -1, -1, -1, 1, 1, -1, 1, 1]

    def test_replace_html_tag(self):

        result = FeatureArray.replace_html_tag(get_training_sample(),
                                               ['html', 'body', 'div', 'ul'], 'ol')
        assert len(result.xpath_array) == len(result.post_array)
        assert result.xpath_array == [['html'],
                                      ['html', 'body'],
                                      ['html', 'body', 'div'],
                                      ['html', 'body', 'div', 'ol'],
                                      ['html', 'body', 'div', 'ol', 'li', '*'],
                                      ['html', 'body', 'div', 'ol', 'li', '*'],
                                      ['html', 'body', 'div', 'ol'],
                                      ['html', 'body', 'div', 'ol', 'li', '*'],
                                      ['html', 'body', 'div', 'ol', 'li', '*']]
        assert result.post_array == [-1, -1, -1, -1, 1, 1, -1, 1, 1]

        result = FeatureArray.replace_html_tag(get_training_sample(),
                                               ['html', 'body', 'div', 'ul', 'li'],
                                               'span')
        assert len(result.xpath_array) == len(result.post_array)
        assert result.xpath_array == [['html'],
                                      ['html', 'body'],
                                      ['html', 'body', 'div'],
                                      ['html', 'body', 'div', 'ul'],
                                      ['html', 'body', 'div', 'ul', 'span', '*'],
                                      ['html', 'body', 'div', 'ul', 'span', '*'],
                                      ['html', 'body', 'div', 'ul'],
                                      ['html', 'body', 'div', 'ul', 'span', '*'],
                                      ['html', 'body', 'div', 'ul', 'span', '*']]
        assert result.post_array == [-1, -1, -1, -1, 1, 1, -1, 1, 1]


    def test_replace_tag(self):
        training_sample = get_training_sample()
        augmented_training_sample = FeatureArray.replace_tag(training_sample, language_model=BISTLM_LANGUAGE_MODEL)
        logger.debug("replace tag: unmodified training sample: \n%s", pp.pformat(training_sample.xpath_array))
        logger.debug("replace tag: augmented sample: \n%s", pp.pformat(augmented_training_sample.xpath_array))
        assert training_sample.post_array == augmented_training_sample.post_array


    def test_insert_sequence(self):
        training_sample = get_training_sample()
        augmented_training_sample = FeatureArray.insert_sequence(training_sample, language_model=BISTLM_LANGUAGE_MODEL)
        logger.debug("insert sequence: unmodified training sample: \n%s", pp.pformat(training_sample.xpath_array))
        logger.debug("insert sequence: augmented sample: \n%s", pp.pformat(augmented_training_sample.xpath_array))
        assert len(augmented_training_sample.xpath_array) == len(augmented_training_sample.post_array)


    def test_multiply_subtrees(self):
        result_trainingsample = FeatureArray.multiply_subtree(get_training_sample(),
                                               ['html', 'body', 'div', 'ul'],
                                               copies=4)
        assert len(result_trainingsample.xpath_array) == len(result_trainingsample.post_array)
        assert result_trainingsample.xpath_array == [
            ['html'],
            ['html', 'body'],
            ['html', 'body', 'div'],
            ['html', 'body', 'div', 'ul'],
            ['html', 'body', 'div', 'ul', 'li', '*'],
            ['html', 'body', 'div', 'ul', 'li', '*'],
            ['html', 'body', 'div', 'ul'],
            ['html', 'body', 'div', 'ul', 'li', '*'],
            ['html', 'body', 'div', 'ul', 'li', '*'],
            ['html', 'body', 'div', 'ul'],
            ['html', 'body', 'div', 'ul', 'li', '*'],
            ['html', 'body', 'div', 'ul', 'li', '*'],
            ['html', 'body', 'div', 'ul'],
            ['html', 'body', 'div', 'ul', 'li', '*'],
            ['html', 'body', 'div', 'ul', 'li', '*']]
        is_post_result = [1 if path[-1] == '*' else -1 for path in
                          result_trainingsample.xpath_array]
        assert result_trainingsample.post_array == is_post_result


    def test_feature_array_augmenter(self):
        training_sample_init = get_training_sample()
        training_sample_augmenter = TrainingSampleAugmenter(training_sample_init, BISTLM_LANGUAGE_MODEL, xpath_cleanup=XPATH_CLEANUP, augmentation_count=100)
        for augmented_training_sample in training_sample_augmenter:
            assert augmented_training_sample.xpath_array


    def test_xpath_merging(self):
        training_sample = get_training_sample()
        post_xpath_counter = FeatureArray.counted_post_xpaths(training_sample.xpath_array, training_sample.post_array)
        assert post_xpath_counter['/html/body/div/ul/li/*'] == 4


    def test_choose_path(self):
        ''' unittests for the choose path method '''
        xpath_array = [[10, 11, 12, 13], [20, 21], [30, 31, 32, 33, 34], [40, 41, 42]]
        xpath, prefix, suffix = choose_path(xpath_array, 3, 1, unittest=True)
        assert xpath == [20, 21]
        assert prefix == [13, '[SEP]', 20]
        assert suffix == ['[SEP]', 30, 31]

        xpath, prefix, suffix = choose_path(xpath_array, 5, 1, unittest=True)
        assert xpath == [20, 21]
        assert prefix == [11, 12, 13, '[SEP]', 20]
        assert suffix == ['[SEP]', 30, 31, 32, 33]

        xpath, prefix, suffix = choose_path(xpath_array, 2, 2, unittest=True)
        assert xpath == [30, 31, 32, 33, 34]
        assert prefix == [32, 33]
        assert suffix == ['[SEP]', 40]

        # borderline cases: suffix or prefix path are too short
        assert (None, None, None) == choose_path(xpath_array, 4, 0, unittest=True)
        assert (None, None, None) == choose_path(xpath_array, 4, 3, unittest=True)


    def test_valid_html_xpath_cleanup(self):
        non_valid_tag = 'norman'
        xpath_array = [
            ['html'],
            ['html', 'body'],
            ['html', 'body', 'div', 'SEP', non_valid_tag]
        ]

        xpath_cleanup = XpathCleanup(VALID_HTML_TAGS)

        cleaned_xpath_array = xpath_cleanup.clean_xpath_with_valid_tags(xpath_array);
        assert cleaned_xpath_array == [
            ['html'],
            ['html', 'body'],
            ['html', 'body', 'div']
        ]


    def test_html_embedding(self):
        max_page_size = 15
        max_tree_len = 5
        training_sample = get_training_sample()
        for xpath in chain(training_sample.xpath_array,
                           repeat([], max_page_size - len(training_sample.xpath_array))):
            xpath = xpath[:max_tree_len]
            for tag in xpath:
                tag_tensor = HTML_EMBEDDING_TOOL[tag]
                print("tensor for tag "+str(tag)+": "+str(tag_tensor)+", shape: "+str(tag_tensor.shape))

    def test_training_sample_to_feature_tensor(self):
        max_page_size = 100
        max_tree_len = 50
        result_shape = (max_page_size, max_tree_len, 20)
        feature_tensor = FeatureTensor.get_feature_tensor(
            training_sample=get_training_sample(),
            html_embedding_tool = HTML_EMBEDDING_TOOL,
            xpath_cleanup=XPATH_CLEANUP,
            max_page_size=max_page_size,
            max_tree_len=max_tree_len)
        logger.debug("test training sample to feature tensor: generated tensor =%s", feature_tensor.tensor)
        assert np.array(feature_tensor.tensor).shape == result_shape
        # test if normalization works properly
        post_array_len = len(feature_tensor.training_sample.post_array)
        assert post_array_len == max_page_size


    def test_counted_posts_xpath(self):
        training_sample = get_training_sample()
        counted_post_xpath = FeatureArray.counted_post_xpaths(training_sample.xpath_array, training_sample.post_array)
        print(pp.pformat(counted_post_xpath))
        # outputs Counter({'/html/body/div/ul/li/*': 4})


if __name__ == '__main__':
    unittest.main()
