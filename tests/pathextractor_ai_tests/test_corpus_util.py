#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on 26.8.2019

.. codeauthor: Norman Süsstrunk <norman.suesstrunk@htwchur.ch>
"""
import os
import pprint
import unittest
import numpy as np

from pathextractorai.augment_features import DEFAULT_MAX_PAGE_SIZE, DEFAULT_MAX_TREE_LEN, TrainingSample
from pathextractorai.corpus_util import CorpusUtil
from pathextractorai.logging import get_logger

pp = pprint.PrettyPrinter(indent=2, width=400)
logger = get_logger(__name__)

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
TEST_FORUM_DATA_DIR = SCRIPT_DIR + '/test_forum_data/'

class TestCorpusUtil(unittest.TestCase):

    @unittest.skip("takes a long time to run, disabled by default")
    def test_corpus_util(self):

        # corpus_util = CorpusUtil(forum_data_dir=TEST_FORUM_DATA_DIR, augmentation_count=5)
        corpus_util = CorpusUtil(augmentation_count=5)

        # read all the html files and generate feature arrays
        augmented_training_samples = corpus_util.read_forum_data_to_augmented_training_samples()
        logger.info("number of generated training samples: %s", len(augmented_training_samples))

        max_page_size, max_tree_length, max_page_size_sample, max_tree_length_sample, average_page_size = CorpusUtil.get_max_pagesize_treelength(augmented_training_samples)

        logger.info("max page size: %s, max tree length: %s, average page size: %s", max_page_size, max_tree_length, average_page_size)
        logger.debug("max page size sample: %s", pp.pformat(max_page_size_sample.xpath_array))
        logger.debug("max tree len sample: %s", pp.pformat(max_tree_length_sample.xpath_array))

        # set the max_page_size & max_tree_len
        max_page_size = 700
        max_tree_length = 50

        # generate the training tensors
        feature_tensors = CorpusUtil.generate_feature_tensors(training_samples=augmented_training_samples,
                                                        max_page_size=max_page_size,
                                                        max_tree_len=max_tree_length)

        # assert dimensions for the x-samples
        result_shape_x = (len(augmented_training_samples), max_page_size, max_tree_length, 20)
        # logger.info("tensors x shape %s", feature_tensors['x'].shape)
        assert feature_tensors['x'].shape == result_shape_x

        # assert the dimensions for the y-samples
        result_shape_y = (len(augmented_training_samples), max_page_size)
        logger.info("tensors y shape %s", feature_tensors['y'].shape)
        assert feature_tensors['y'].shape == result_shape_y
        for feature_tensor_y in feature_tensors['y']:
            # logger.info(np.array(feature_tensor_y).shape)
            assert np.array(feature_tensor_y).shape == (max_page_size,)

        # dump the training tensors
        CorpusUtil.pickle_dump_feature_tensors(feature_tensors=feature_tensors)

        # load the training tensors
        loaded_feature_tensors = CorpusUtil.pickle_load_feature_tensors()

        logger.info("loaded tensors x shape %s", loaded_feature_tensors['x'].shape)
        logger.info("loaded tensors y shape %s", loaded_feature_tensors['x'].shape)

        assert feature_tensors['x'].shape == loaded_feature_tensors['x'].shape
        assert feature_tensors['y'].shape ==  loaded_feature_tensors['y'].shape
        assert len(feature_tensors['training_samples']) == len(loaded_feature_tensors['training_samples'])

    def test_calc_max_page_size_tree_length(self):
        xpath_array_1 = [[1, 2, 3], [1, 2, 3, 4]]
        xpath_array_2 = [[1, 2, 3, 4, 5, 6], [1, 2, 3, 4], [1, 2]]
        post_array = [1, 2]
        training_samples = [
            TrainingSample(xpath_array=xpath_array_1, post_array=post_array, url = "url")
            ,TrainingSample(xpath_array=xpath_array_2, post_array=post_array, url = "url")]
        max_page_size, max_tree_length, max_page_size_sample, max_tree_length_sample, average_page_size = CorpusUtil.get_max_pagesize_treelength(training_samples)
        assert max_page_size == 3
        assert max_tree_length == 6

    def test_read_evaluation_corpus(self):
        corpus_util = CorpusUtil()
        eval_json_datas = corpus_util.read_evaluation_samples()
        for eval_json_data in eval_json_datas:
             assert eval_json_data['dom_tree']

if __name__ == '__main__':
    unittest.main()
