#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os

from collections import namedtuple

HtmlPage = namedtuple('HtmlPage', ('url', 'html_content'))

'''
the data dir (tests/data/forumData) with the crawled data from the forums in json format
json format:

    {
        'url':'[url]'
        'html':'[html content]'
        'content-id':['content_id from the ds9 crawler']
    }
'''
FORUM_DATA_DIR = os.path.join(os.path.dirname(__file__), '../../forum_data/')


def load_webpage_from_json_file(json_file):
    '''
    loads the given json file
    @return a instance of class:: html_page
    '''
    json_file_path = FORUM_DATA_DIR + '/' + json_file
    webpage_json = json.load(open(json_file_path))

    # classifier = ContentClassifier()

    return HtmlPage(webpage_json['url'], html_content=webpage_json['html'])


def generate_example_html_page():
    # generate simple html page
    example_html = '<html><body><div><ul><li>list1</li><li>list2</li></ul><ul><li>list3</li><li>list4</li></ul></div></body><html>'
    return HtmlPage(url="http://test.com", html_content=example_html)
