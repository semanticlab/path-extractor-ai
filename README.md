# Path Extractor AI 



## Scripts 


### Generating the corpus 
[pathextractor-generate-corpus-pickle.py](scripts/pathextractor-generate-corpus-pickle.py): reads all the html files from [forum_data](forum_data/) and calculates the features and saves it to a pickle file

### Training the model
* [pathextractor-train-model.py](scripts/pathextractor-train-model.py): trains the keras model with the complete corpus and runs an evaluation. 

To use a subset of the training data (for testing / developing on local machine), change the script as following: 

```python
# use the full corpus
# corpus_util = CorpusUtil(augmentation_count=50)

# use the forum test data
corpus_util = CorpusUtil(forum_data_dir=TEST_FORUM_DATA_DIR, augmentation_count=50)
```

The generated model will be saved in the scripts-directory with the filename ***pathextractor_keras_model***


The directory [forum_data/train](forum_data/train) contains crawled forum pages from bb_press and phpBB forums. The posts and the corresponding gold xpaths are 



### Evaluating the trained model

* [pathextractor-evaluate-model.py](scripts/pathextractor-evaluate-model.py): Evaluates the trained model.

For the evaluation, examples from about 30 forums are saved in the directory [forum_data/train](forum_data/train). 

## Test Data / Unit-Test for Post Extractors 

DS9 Test Crawl for the following urls: 

* http://forum.parkinson.org/ (JSON-LD)
* https://www.mumsnet.com/info/search?q=angelman (JSON-LD)
* https://forum.parkinsons.org.uk/ (bb-press xpath extractor)
* http://forums.maladiesraresinfo.org/search.php?keywords=Angelman (phpbb forum)

[forum-data crawl data (json-files)](forum-data)

# AI Approach

# Development Setup 

## Setting PYTHONPATH 


Example if the project dir is /home/suesstnorma1/git/path-extractor-ai/

```
export PYTHONPATH=/home/suesstnorma1/git/path-extractor-ai/src
```

## Running all unit tests 

```
python3 -m pytest
```

# Installation Keras & Tensor

## Install Keras 

```
sudo pip3 install keras  
```

## TensorFlow as Backend 

```
# upgrade pip
pip3 install --upgrade pip

# Current stable release for CPU-only
pip install tensorflow
```


# Asking the model 

load the model 

```python

max_page_size = 500
max_tree_length = 50

# load the model
from pathextractorai.pathextractor_keras_model import PathExtractorKerasModel
pathextractor_model = PathExtractorKerasModel.load_model_file()
#
 load the corpus / trainingsamples  
from pathextractorai.corpus_util import CorpusUtil
corpus_util = CorpusUtil(augmentation_count=5)
augmented_training_samples = corpus_util.read_forum_data_to_augmented_training_samples()

# generate the tensors 
feature_tensors = CorpusUtil.generate_feature_tensors(training_samples=augmented_training_samples, max_page_size=max_page_size, max_tree_len=max_tree_length)

# predict a training sample
predicted_post_arrays = pathextractor_model.predict(feature_tensors['x'])

print(predicted_post_arrays[0])

>>> print(predicted_post_arrays[2])
[0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 1. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.]
```
